//
//  BRSettingsViewController.m
//  iOSFonts
//
//  Created by Razvan Balazs on 15/10/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRSettingsViewController.h"

@interface BRSettingsViewController ()
@end

@implementation BRSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Some Settings";
    //load default values
    self.layoutSegmControl.selectedSegmentIndex =  self.textAlignment == NSTextAlignmentLeft ? 0:1;
    self.toggleInverseCharactersUISwitch.on =  self.reverseCharacters;
    self.toggleReverseSortOrderUISwitch.on =  !self.ascendingSort;
    if (self.sortType >= 0) {
        self.sortSegmControl.selectedSegmentIndex  =  self.sortType;
    }else{
        [self.sortSegmControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    }


}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)layoutSegmControlAction:(id)sender {
}

- (IBAction)sortSegmControlAction:(id)sender {
}

- (IBAction)toggleInverseCharactersAction:(id)sender {

}

- (IBAction)reverseSortAction:(id)sender {
    
}

- (IBAction)doneAction:(id)sender {
    [self.delegate settingsUpdatedWithJustified:self.layoutSegmControl.selectedSegmentIndex == 0 ? NSTextAlignmentLeft:NSTextAlignmentRight
                                charactersOrder:[self.toggleInverseCharactersUISwitch isOn]
                                    andSortType:self.sortSegmControl.selectedSegmentIndex
                                      ascending:![self.toggleReverseSortOrderUISwitch isOn]];
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)resetAction:(id)sender {
    [self.delegate resetToInitialList];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
