//
//  BRAlphabetViewController.h
//  iOSFonts
//
//  Created by Razvan Balazs on 16/10/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRAlphabetViewController : UIViewController
@property (nonatomic,strong) UIFont *font;
@property (weak, nonatomic) IBOutlet UILabel *alphabetLabel;

@end
