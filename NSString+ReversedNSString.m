//
//  NSString+ReversedNSString.m
//  FontCatalog
//
//  Created by Razvan Balazs on 10/7/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "NSString+ReversedNSString.h"

@implementation NSString (ReversedNSString)

- (NSString*)reversedNSString{

    NSMutableString *reversedString = [NSMutableString stringWithCapacity:self.length];
    [self enumerateSubstringsInRange:NSMakeRange(0, self.length)
                             options:(NSStringEnumerationReverse | NSStringEnumerationByComposedCharacterSequences)
                          usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                              [reversedString appendString:substring];
                          }];
    return reversedString;
}
@end
