//
//  BRFontObject.h
//  iOSFonts
//
//  Created by Razvan Balazs on 16/10/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BRFontObject : NSManagedObject

@property (nonatomic, retain) NSString * fontName;
@property (nonatomic, retain) NSNumber * length;
@property (nonatomic, retain) NSNumber * ratings;
@property (nonatomic, retain) NSNumber * frequencyOfUse;

@end
