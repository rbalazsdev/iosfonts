//
//  BRFontObject.m
//  iOSFonts
//
//  Created by Razvan Balazs on 16/10/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRFontObject.h"


@implementation BRFontObject

@dynamic fontName;
@dynamic length;
@dynamic ratings;
@dynamic frequencyOfUse;

@end
