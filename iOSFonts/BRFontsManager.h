//
//  BRFontsManager.h
//  iOSFonts
//
//  Created by Razvan Balazs on 10/15/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface BRFontsManager : NSObject
@property (nonatomic, strong) NSArray* fonts;

- (NSMutableArray*)alphaSort:(NSArray*)unsortedArray ascending:(BOOL)ascending;
- (NSMutableArray*)characterCountSort:(NSArray*)unsortedArray ascending:(BOOL)ascending;
- (NSMutableArray*)lengthSort:(NSArray*)unsortedArray ascending:(BOOL)ascending;

@end
