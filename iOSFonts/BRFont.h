//
//  BRFont.h
//  iOSFonts
//
//  Created by Razvan Balazs on 10/14/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BRFont : NSObject

@property (nonatomic, copy) NSString *fontName;
@property (nonatomic, copy) NSString *inversedfontName;
@property (nonatomic, strong) UIFont *font;
@property(nonatomic) NSUInteger fontLength;

- (id)initWithFontName:(NSString*)fontFamilyName;

@end
