//
//  BRFont.m
//  iOSFonts
//
//  Created by Razvan Balazs on 10/15/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRFont.h"
#import "NSString+ReversedNSString.h"

@interface BRFont ()
@end

@implementation BRFont

- (id)initWithFontName:(NSString*)fontFamilyName {

    self = [super init];
    if (self != nil) {
        _fontName = fontFamilyName;
        _inversedfontName = [fontFamilyName reversedNSString];
        _font = [UIFont fontWithName:fontFamilyName size:18];
        _fontLength = [_fontName sizeWithFont:_font].width;
    }
    return self;
}

@end

