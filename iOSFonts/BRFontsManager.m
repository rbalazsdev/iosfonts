//
//  BRFontsManager.m
//  iOSFonts
//
//  Created by Razvan Balazs on 10/14/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//
#import "BRFont.h"
#import "BRFontsManager.h"

@interface BRFontsManager (){
    NSArray *fontFamilies;
}
@end
@implementation BRFontsManager


- (id)init{
    self = [super init];
    if (self) {
        fontFamilies = [NSArray arrayWithArray:[UIFont familyNames]];
        NSMutableArray *arrayOfFonts = [[NSMutableArray alloc] initWithCapacity:0];
        for (id fontFamily in fontFamilies) {
            BRFont *fontObject = [[BRFont alloc] initWithFontName:fontFamily];
            [arrayOfFonts addObject:fontObject];
        }
        _fonts = [NSArray arrayWithArray:arrayOfFonts];
    }
    return self;
}

- (NSMutableArray*)alphaSort:(NSArray*)unsortedArray ascending:(BOOL)ascending {

    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"fontName"
                                                               ascending:ascending
                                                                selector:@selector(localizedCaseInsensitiveCompare:)];
   return [NSMutableArray arrayWithArray:[unsortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];
    
}

- (NSMutableArray*)characterCountSort:(NSArray*)unsortedArray ascending:(BOOL)ascending {

    NSSortDescriptor *descriptor =
    [[NSSortDescriptor alloc] initWithKey:@"fontName"
                                ascending:ascending
                               comparator:^NSComparisonResult(id obj1, id obj2) {
                                  NSNumber *obj1Length = [NSNumber numberWithInt:((NSString*)obj1).length];
                                  NSNumber *obj2Length = [NSNumber numberWithInt:((NSString*)obj2).length];
                                  return [obj1Length compare: obj2Length];
                              }];
    return  [NSMutableArray arrayWithArray:[unsortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];

}

- (NSMutableArray*)lengthSort:(NSArray*)unsortedArray ascending:(BOOL)ascending {
    
    NSSortDescriptor *descriptor =
    [[NSSortDescriptor alloc] initWithKey:nil
                                ascending:ascending
                               comparator:^NSComparisonResult(id obj1, id obj2) {
                                   NSNumber *obj1Length = [NSNumber numberWithInt:((BRFont*)obj1).fontLength];
                                   NSNumber *obj2Length = [NSNumber numberWithInt:((BRFont*)obj2).fontLength];
                                   return [obj1Length compare: obj2Length];
                               }];
    return  [NSMutableArray arrayWithArray:[unsortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];
}
@end
