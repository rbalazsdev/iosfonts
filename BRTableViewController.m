//
//  BRTableViewController.m
//  iOSFonts
//
//  Created by Razvan Balazs on 10/15/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//
#import "BRFont.h"
#import "BRFontsManager.h"
#import "BRSettingsViewController.h"
#import "BRAlphabetViewController.h"
#import "BRAppDelegate.h"
#import "BRTableViewController.h"

@interface BRTableViewController () <BRSettingViewControllerDelegate>
{
    NSTextAlignment cellTextAlignment;
    BOOL reverseCharacters;
    BOOL ascendingSort;
    NSInteger sortType;
}
@property (nonatomic, strong) NSMutableArray* allFonts;
@property (nonatomic, strong) NSMutableArray* searchResults;
@property (nonatomic, strong) BRFontsManager *fontsMgr;

@end

@implementation BRTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.navigationItem.rightBarButtonItem = self.editButtonItem;

    _fontsMgr = [BRFontsManager new];
    _allFonts = [NSMutableArray arrayWithArray:[_fontsMgr fonts]];
    _searchResults = [NSMutableArray arrayWithCapacity:[_allFonts count]];

    [self defaultSettingsForLayoutAndSort];
    
    BRAppDelegate *appDelegate= (BRAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext =  appDelegate.managedObjectContext;
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.searchResults count];
    } else {
        return self.allFonts.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    BRFont* cellFont;
    if (tableView == self.searchDisplayController.searchResultsTableView)	{
        cellFont = [self.searchResults objectAtIndex:indexPath.row];
    }else {
        cellFont = [self.allFonts objectAtIndex:indexPath.row];
    }

    cell.textLabel.font = cellFont.font;
    cell.textLabel.text = reverseCharacters ? cellFont.inversedfontName : cellFont.fontName;
    [cell.textLabel setTextAlignment:cellTextAlignment];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.allFonts removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    id item = [self.allFonts objectAtIndex:fromIndexPath.row];
    [self.allFonts removeObjectAtIndex:fromIndexPath.row];
    [self.allFonts insertObject:item atIndex:toIndexPath.row];
    sortType = -1;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView)	{
        [self performSegueWithIdentifier:@"showAlphabetViewSegue" sender:[self.searchDisplayController.searchResultsTableView cellForRowAtIndexPath:indexPath]];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showSettingsSegue"]){
        BRSettingsViewController *settingsVc = (BRSettingsViewController *)segue.destinationViewController;
        settingsVc.delegate = self;
        settingsVc.textAlignment = cellTextAlignment;
        settingsVc.reverseCharacters = reverseCharacters;
        settingsVc.sortType = sortType;
        settingsVc.ascendingSort = ascendingSort;
    }
    if([segue.identifier isEqualToString:@"showAlphabetViewSegue"]){
        BRAlphabetViewController *alphabetVc = (BRAlphabetViewController *)segue.destinationViewController;
        alphabetVc.font = [[(UITableViewCell*)sender textLabel] font];
    }
}

#pragma mark - BRSettingControllerDelegate
- (void)settingsUpdatedWithJustified:(NSTextAlignment)textAlignment charactersOrder:(BOOL)inverse andSortType:(NSUInteger)newSortType ascending:(BOOL)ascending{
    switch (newSortType) {
        case 0:
            self.allFonts  = [self.fontsMgr alphaSort:self.allFonts ascending:ascending];
            break;
        case 1:
            self.allFonts  = [self.fontsMgr characterCountSort:self.allFonts ascending:ascending];
            break;
        case 2:
            self.allFonts  = [self.fontsMgr lengthSort:self.allFonts ascending:ascending];
            break;
            
        default:
            break;
    }
    cellTextAlignment = textAlignment;
    reverseCharacters = inverse;
    ascendingSort = ascending;
    sortType = newSortType;
    
    [self.tableView reloadData];
}
- (void)resetToInitialList{
    self.allFonts = [NSMutableArray arrayWithArray:[self.fontsMgr fonts]];
    [self defaultSettingsForLayoutAndSort];
    [self.tableView reloadData];

}

- (void)defaultSettingsForLayoutAndSort{

    cellTextAlignment = NSTextAlignmentLeft;
    reverseCharacters = NO;
    ascendingSort = YES;
    sortType = -1; // alpha

}


#pragma mark - Content Filtering
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	[self.searchResults removeAllObjects];
    for (BRFont *brFont in self.allFonts) {
		if ([scope isEqualToString:@"All"] || [brFont.fontName isEqualToString:scope]) {
			NSComparisonResult result = [brFont.fontName compare:searchText
                                                         options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)
                                                           range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame) {
				[self.searchResults addObject:brFont];
            }
		}
	}
    
}

#pragma mark UISearchDisplayController Delegate Methods
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:@"All"];
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:@"All"];
    return YES;
}
@end
