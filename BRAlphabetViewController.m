//
//  BRAlphabetViewController.m
//  iOSFonts
//
//  Created by Razvan Balazs on 16/10/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRAlphabetViewController.h"

@interface BRAlphabetViewController ()

@end

@implementation BRAlphabetViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSMutableArray *alphabetArray = [NSMutableArray new];
    for(char a = 'A'; a <= 'z'; a++){
        [alphabetArray addObject:[NSString stringWithFormat:@"%c", a]];
    }
    [self.alphabetLabel setFont:self.font];
    self.alphabetLabel.text = [alphabetArray componentsJoinedByString:@"  "];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
