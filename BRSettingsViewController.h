//
//  BRSettingsViewController.h
//  iOSFonts
//
//  Created by Razvan Balazs on 15/10/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewControllerB;

@protocol BRSettingViewControllerDelegate <NSObject>

- (void)settingsUpdatedWithJustified:(NSTextAlignment)textAlignment
                     charactersOrder:(BOOL)inverse
                        andSortType:(NSUInteger)sortType
                           ascending:(BOOL)ascending;
- (void)resetToInitialList;

@end

@interface BRSettingsViewController : UIViewController {

}
@property (nonatomic, weak) id <BRSettingViewControllerDelegate> delegate;


@property(strong,nonatomic) NSMutableDictionary *settingsDict;
@property (weak, nonatomic) IBOutlet UISegmentedControl *layoutSegmControl;
@property (weak, nonatomic) IBOutlet UISwitch *toggleInverseCharactersUISwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortSegmControl;
@property (weak, nonatomic) IBOutlet UISwitch *toggleReverseSortOrderUISwitch;

//initialize these values when details controller is pushed
@property(nonatomic)    NSTextAlignment textAlignment;
@property(nonatomic)    BOOL reverseCharacters;
@property(nonatomic)    BOOL ascendingSort;
@property(nonatomic)    NSInteger sortType;

- (IBAction)layoutSegmControlAction:(id)sender;
- (IBAction)sortSegmControlAction:(id)sender;
- (IBAction)toggleInverseCharactersAction:(id)sender;
- (IBAction)reverseSortAction:(id)sender;

- (IBAction)doneAction:(id)sender;

- (IBAction)resetAction:(id)sender;
@end
