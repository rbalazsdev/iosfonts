//
//  NSString+ReversedNSString.h
//  FontCatalog
//
//  Created by Razvan Balazs on 10/7/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ReversedNSString)
- (NSString*)reversedNSString;

@end
