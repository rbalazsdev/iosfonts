//
//  BRTableViewController.h
//  iOSFonts
//
//  Created by Razvan Balazs on 10/15/13.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRTableViewController : UITableViewController <UISearchBarDelegate,UISearchDisplayDelegate>
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
